﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.Ships
{
    class Dart : Vaisseau
    {
        public Dart() : base(10, 3)
        {
            Armurerie armurerie = new Armurerie();
            this.AddWeapon(armurerie.GetWeaponByName("Dart Weapon"));
        }

        public override void Attack(Vaisseau target)
        {
            foreach(Arme weap in this.Armes)
            {
                if (weap.WeaponType == Type.Direct)
                {
                    int dmg = weap.DmgFromShot();
                    weap.ReloadCounter = 1;
                    target.TakeShot(dmg);
                }
            }
        }

        public override string ToString()
        {
            return "Dart " + base.ToString();
        }
    }
}
