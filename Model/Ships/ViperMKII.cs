﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.Ships
{
    class ViperMKII : Vaisseau
    {
        public ViperMKII() : base(10, 15)
        {
            Armurerie armurerie = new Armurerie();
            this.AddWeapon(armurerie.GetWeaponByName("Mitrailleuse"));
            this.AddWeapon(armurerie.GetWeaponByName("EMG"));
            this.AddWeapon(armurerie.GetWeaponByName("Missile"));
        }

        public override void Attack(Vaisseau target)
        {
            Arme toFire = Armes[0];
            double dmg = 0;
            foreach (Arme weap in this.Armes)
            {
                if (weap.ReloadCounter <= 1 && weap.ExpectedDmg() >= dmg)
                {
                    dmg = weap.ExpectedDmg();
                    toFire = weap;
                }
            }

            toFire.ReloadCounter = 1;
            target.TakeShot(toFire.DmgFromShot());
        }

        public override string ToString()
        {
            return "ViperMKII " + base.ToString();
        }
    }
}
