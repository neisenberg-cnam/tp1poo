﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.Ships
{
    class BWings : Vaisseau
    {
        public BWings() : base(30, 0)
        {
            Armurerie armurerie = new Armurerie();
            this.AddWeapon(armurerie.GetWeaponByName("B-Wings Weapon"));
        }

        public override void Attack(Vaisseau target)
        {
            foreach (Arme weap in this.Armes)
            {
                if (weap.WeaponType == Type.Explosif)
                {
                    int dmg = weap.DmgFromShot();
                    weap.ReloadCounter = 1;
                    target.TakeShot(dmg);
                }
            }
        }

        public override string ToString()
        {
            return "B-Wings " + base.ToString();
        }
    }
}
