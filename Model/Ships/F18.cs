﻿using SpaceInvaderGame.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaderGame.Ships
{
    class F18 : Vaisseau, IAptitude
    {
        public F18() : base(15, 0)
        {}

        public void Utilise(List<Vaisseau> list)
        {
            if(list[1] == this)
            {
                list[0].TakeShot(10);
                list.Remove(this);
            }
        }

        public override void Attack(Vaisseau target)
        {}

        public override string ToString()
        {
            return "F-18 " + base.ToString();
        }
    }
}
