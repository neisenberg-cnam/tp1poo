﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.ships
{
    class Rocinante : Vaisseau
    {
        public Rocinante() : base(3, 5)
        {
            Armurerie armurerie = new Armurerie();
            this.AddWeapon(armurerie.GetWeaponByName("Rocinante Weapon"));
        }

        public override void Attack(Vaisseau target)
        {
            foreach (Arme weap in this.Armes)
            {
                int dmg = weap.DmgFromShot();
                target.TakeShot(dmg);
            }
        }

        public override string ToString()
        {
            return "Rocinante " + base.ToString();
        }
    }
}
