﻿using SpaceInvaderGame.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.Ships
{
    class Tardis : Vaisseau, IAptitude
    {
        public Tardis() : base(1, 0)
        { }

        public void Utilise(List<Vaisseau> list)
        {
            Random _random = new Random();
            int first_index = _random.Next(1, list.Count);
            int second_index = _random.Next(1, list.Count);
            Vaisseau tmp = list[first_index];
            list[first_index] = list[second_index];
            list[second_index] = tmp;
        }

        public override void Attack(Vaisseau target)
        { }

        public override string ToString()
        {
            return "Tardis " + base.ToString();
        }
    }
}
