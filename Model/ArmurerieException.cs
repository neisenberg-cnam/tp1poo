using System;

namespace SpaceInvaderGame
{
    class ArmurerieException : Exception
    {
        public ArmurerieException() { }

        public ArmurerieException(string message)
            : base(message) { }

        public ArmurerieException(string message, Exception inner)
            : base(message, inner) { }


    }
}