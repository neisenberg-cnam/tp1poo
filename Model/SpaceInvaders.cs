using System;
using System.Collections.Generic;
using System.Linq;
using SpaceInvaderGame.Interface;
using SpaceInvaderGame.ships;
using SpaceInvaderGame.Ships;
using SpaceInvaderGame.Util;

namespace SpaceInvaderGame
{
    
    
    class SpaceInvaders
    {
        private List<Joueur> Joueurs;
        private readonly Armurerie Armory;
        private List<Vaisseau> Ships;
        public SpaceInvaders()
        {
            this.Joueurs = InitPlayers();
            Ships = InitShips();
            Armory = new Armurerie();
        }

        public Armurerie GetArmory()
        {
            return Armory;
        }

        private List<Vaisseau> InitShips()
        {
            List<Vaisseau> list = new List<Vaisseau>();

            var ship = PlayerNumber(0).GetVaisseau();
            list.Add(ship);

            List<Vaisseau> ennemies = new List<Vaisseau>();
            BWings bwings = new BWings();
            Dart dart = new Dart();
            F18 f18 = new F18();
            Rocinante rocinante = new Rocinante();
            Tardis tardis = new Tardis();
            ennemies.Add(bwings);
            ennemies.Add(dart);
            ennemies.Add(f18);
            ennemies.Add(rocinante);
            ennemies.Add(tardis);

            Random _random = new Random();
            Shuffler.Shuffle<Vaisseau>(ennemies, _random);

            list.AddRange(ennemies);

            return list;
        }
        
        private List<Joueur> InitPlayers()
        {
            List<Joueur> liste = new List<Joueur>();
            // Joueur j1 = new Joueur("Doe", "John", "TheJohnDoer");
            // Joueur j2 = new Joueur("Doe", "Jane", "TheJaneDoer");
            Joueur j3 = new Joueur("Eisenberg", "Nicolas", "neisenbe");
            // liste.Add(j1);
            // liste.Add(j2);
            liste.Add(j3);
            return liste;
        }

        public void PlayerList()
        {
            Joueurs.ForEach(delegate(Joueur j)
            {
                Console.WriteLine(j);
            });
        }

        public void ShipList()
        {
            Ships.ForEach(delegate (Vaisseau v)
            {
                Console.WriteLine(v);
            });
        }


        public Joueur PlayerNumber(int index)
        {
            return Joueurs[index];
        }

        public void PlayATurn()
        {
            bool hasAttacked = false;
            foreach(var ship in Ships.ToList())
            {
                if (ship.CurrentBouclier != ship.Bouclier)
                {
                    ship.CurrentBouclier = Math.Min(ship.Bouclier, ship.CurrentBouclier + 2);
                }
                    
                if (ship is IAptitude)
                {
                    ((IAptitude)ship).Utilise(Ships);
                }
                    
            }
            for(int i = 1; i < Ships.Count; i++)
            {
                Random _random = new Random();
                if (_random.Next(i, Ships.Count) == i && !hasAttacked)
                {
                    Ships[0].Attack(Ships[_random.Next(1, Ships.Count)]);
                    hasAttacked = true;
                }
                if (Ships[i].CurrentStructure > 0)
                {
                    Ships[i].Attack(Ships[0]);
                }
            }
            for (int i = 1; i < Ships.Count; i++)
            {
                if (Ships[i].CurrentStructure <= 0)
                {
                    Ships.Remove(Ships[i]);
                }
            }
        }
    }
    
}