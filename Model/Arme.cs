using System;

namespace SpaceInvaderGame
{
    enum Type
    {
        Direct,
        Explosif,
        Guidé
    }
    
    class Arme
    {
        public string Nom { get; set; }
        public int DmgMin {get; set;}
        public int DmgMax {get; set;}

        public double ReloadingTime { get; set; }
        public double ReloadCounter { get; set; }
        public Type WeaponType {get; set;}

        public Arme(string nom, int min, int max, Type type, double nb_turns)
        {
            Nom = nom;
            DmgMax = max;
            DmgMin = min;
            WeaponType = type;
            ReloadingTime = nb_turns;
            ReloadCounter = nb_turns;
        }

        public override string ToString()
        {
            return Nom + " : " + WeaponType + " (" + DmgMin + "-" + DmgMax + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (!(obj is Arme))
            {
                return false;
            }
            return this.Nom == ((Arme)obj).Nom;
        }

        public int DmgFromShot()
        {
            ReloadCounter--;
            if (ReloadCounter > 0)
            {
                return 0;
            }
            Random _random = new Random();
            int dmg = _random.Next(DmgMin, DmgMax);
            if(WeaponType == Type.Direct)
            {
                int dice = _random.Next(1, 10);
                ReloadCounter = ReloadingTime;
                return dice == 1 ? 0 : dmg;
            }
            else if (WeaponType == Type.Explosif)
            {
                int dice = _random.Next(1, 4);
                ReloadCounter = ReloadingTime * 2;
                return dice == 1 ? 0 : dmg * 2;
            }
            else if (WeaponType == Type.Guidé)
            {
                ReloadCounter = ReloadingTime;
                return DmgMin;
            }
            return 0;
        }

        public int AvgDmg()
        {
            return (DmgMin + DmgMax) / 2;
        }

        public double ExpectedDmg()
        {
            if (WeaponType == Type.Direct)
            {
                return AvgDmg() * 0.9;
            }
            else if (WeaponType == Type.Explosif)
            {
                return AvgDmg() * 2 * 0.75;
            }
            else if (WeaponType == Type.Guidé)
            {
                return DmgMin;
            }
            return 0;
        }
    }
}