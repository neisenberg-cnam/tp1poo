using System.Globalization;
using SpaceInvaderGame.Ships;

namespace SpaceInvaderGame
{
    class Joueur
    {
        public readonly string Nom;
        public readonly string Prenom;
        public string Pseudo {set; get;}
        public Vaisseau Ship { get; private set; }
        private Armurerie Armory;

        public Joueur(string nom, string prenom, string pseudo)
        {
            this.Nom = StringFormatter(nom);
            this.Prenom = StringFormatter(prenom);
            this.Pseudo = pseudo;
            Ship = new ViperMKII();
        }

        public string GetIdentity()
        {
            return Prenom + " " + Nom;
        }

        public void SetShip(Vaisseau ship)
        {
            Ship = ship;
        }

        public Vaisseau GetVaisseau()
        {
            return Ship;
        }

        static string StringFormatter(string toFormat)
        {
            TextInfo textInfo = new CultureInfo(CultureInfo.CurrentCulture.Name, false).TextInfo;
            return textInfo.ToTitleCase(toFormat.ToLower());
        }

        public override string ToString()
        {
            return Pseudo + " (" + GetIdentity() + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (!(obj is Joueur))
            {
                return false;
            }
            return this.Pseudo == ((Joueur)obj).Pseudo;
        }
    }
}