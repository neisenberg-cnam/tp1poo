using System.Collections.Generic;
using System;

namespace SpaceInvaderGame
{
    abstract class Vaisseau
    {
        public int Structure { get; set;}
        public int CurrentStructure { get; set; }
        public int Bouclier { get; set;}
        public int CurrentBouclier { get; set; }

        protected List<Arme> Armes;

        public string ImgPath { get; set; }

        public Vaisseau(int structure, int bouclier)
        {
            Structure = structure;
            Bouclier = bouclier;
            CurrentBouclier = bouclier;
            CurrentStructure = structure;
            Armes = new List<Arme>();
        }

        public void AddWeapon(Arme arme)
        {
            try
            {
                Armurerie armory = new Armurerie();
                if (!armory.Contains(arme))
                {
                    throw (new ArmurerieException("ArmurerieException: Tried to add a weapon to a ship when the weapon isn't in the armory"));
                }
                else 
                {
                    if (Armes.Count < 6)
                    {
                        Armes.Add(arme);
                    }
                }
            }
            catch (ArmurerieException aex)
            {   
                Console.WriteLine(aex.Message.ToString());   
            }
        }

        public override string ToString()
        {
            return "Struct (curr/max) : " + CurrentStructure + "/" + Structure + " Bouclier (curr/max) : " + CurrentBouclier + "/" + Bouclier;
        }

        public void RemoveWeapon(Arme arme)
        {
            Armes.Remove(arme);
        }

        public List<Arme> WeaponList()
        {
            // Armes.ForEach(delegate(Arme weap)
            // {
            //     Console.WriteLine(weap);
            // });
            return Armes;
        }

        public float AverageDamage()
        {
            float sum_min = 0;
            float sum_max = 0;
            Armes.ForEach(delegate(Arme weap)
            {
                sum_max += weap.DmgMax;
                sum_min += weap.DmgMin;
            });
            float avg_min = sum_min / Armes.Count;
            float avg_max = sum_max / Armes.Count;
            return (avg_max + avg_min) / 2;
        }

        public void TakeShot(int shot_dmg)
        {
            if (CurrentBouclier > 0)
            {
                int max = Math.Max(shot_dmg, CurrentBouclier);
                if (max == shot_dmg)
                {
                    shot_dmg -= CurrentBouclier;
                    CurrentBouclier = 0;
                }
                else
                {
                    CurrentBouclier -= shot_dmg;
                    shot_dmg = 0;
                }
            }
            CurrentStructure -= shot_dmg;
        }

        public abstract void Attack(Vaisseau target);
    }
}