﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaderGame.Interface
{
    interface IAptitude
    {
        public void Utilise(List<Vaisseau> list);
    }
}
