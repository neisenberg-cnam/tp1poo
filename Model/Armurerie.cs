using System.Collections.Generic;
using System;
using System.Reflection.Metadata.Ecma335;

namespace SpaceInvaderGame
{
    
    class Armurerie
    {
        private List<Arme> Armes;

        public Armurerie()
        {
            Init();
        }

        public void Init()
        {
            List<Arme> liste = new List<Arme>();

            Arme dartWeapon = new Arme("Dart Weapon", 2, 3, Type.Direct, 1);
            liste.Add(dartWeapon);
            Arme bwingsWeapon = new Arme("B-Wings Weapon", 1, 8, Type.Explosif, 1.5);
            liste.Add(bwingsWeapon);
            Arme rocinanteWeapon = new Arme("Rocinante Weapon", 3, 3, Type.Guidé, 2);
            liste.Add(rocinanteWeapon);
            Arme mitrailleuse = new Arme("Mitrailleuse", 2, 3, Type.Direct, 1);
            Arme emg = new Arme("EMG", 1, 7, Type.Explosif, 1.5);
            Arme missile = new Arme("Missile", 4, 100, Type.Guidé, 4);
            liste.Add(emg);
            liste.Add(missile);
            liste.Add(mitrailleuse);

            this.Armes = liste;
        }

        public List<Arme> WeaponList()
        {
            //Armes.ForEach(delegate(Arme weap)
            //{
            //    Console.WriteLine(weap);
            //});
            return Armes;
        }

        public Arme GetNthWeapon(int index)
        {
            return Armes[index];
        }

        public Arme GetWeaponByName(string name)
        {
            foreach (Arme weap in Armes)
            {
                if (weap.Nom == name)
                    return weap;
            }
            return null;
        }

        public bool Contains(Arme weap)
        {
            return Armes.Contains(weap);
        }
    }
}