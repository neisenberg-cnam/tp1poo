﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TP4.ViewModel;

namespace TP4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PlayerViewModel PVM = PlayerViewModel.Instance;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = PVM;
            addWeapBtn.IsEnabled = false;
            delWeap.IsEnabled = false;
            delUsr.IsEnabled = false;
            addImg.IsEnabled = false;
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = this.listView.SelectedIndex;
            if (index >= 0)
            {
                PVM.changeCurrentPlayer(index);
                shipNom.Text = PVM.getPlayerShipName(index);
                shipAvgDmg.Text = PVM.getPlayerShipStruc(index).ToString();
                weapList.ItemsSource = PVM.getWeaponList();
                addWeapBtn.IsEnabled = true;
                addImg.IsEnabled = true;
                delWeap.IsEnabled = true;
                delUsr.IsEnabled = true;
            }
            else
            {
                addWeapBtn.IsEnabled = false;
                addImg.IsEnabled = false;
                delWeap.IsEnabled = false;
                delUsr.IsEnabled = false;
            }
        }


        private void AddPlayer(object sender, RoutedEventArgs e)
        {
            AddPlayerWindow window = new AddPlayerWindow();
            window.ShowDialog();
            this.listView.Items.Refresh();
        }

        private void RemovePlayer(object sender, RoutedEventArgs e)
        {
            PVM.removePlayer(this.listView.SelectedIndex);
            this.listView.Items.Refresh();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == true)
            {
                PVM.changeCurrentImage(file.FileName);
            }
                
        }

        private void AddWeapon(object sender, RoutedEventArgs e)
        {
            AddAWeaponWindow window = new AddAWeaponWindow();
            window.ShowDialog();
            weapList.ItemsSource = null;
            weapList.ItemsSource = PVM.getWeaponList();
        }

        private void DelWeapon(object sender, RoutedEventArgs e)
        {
            int index = weapList.SelectedIndex;
            PVM.delWeaponToCurrent(index);
            weapList.ItemsSource = null;
            weapList.ItemsSource = PVM.getWeaponList();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string message = "Function not yet implemented";
            string caption = "Not yet implemented warning";
            MessageBoxImage icon = MessageBoxImage.Warning;
            MessageBoxButton ok = MessageBoxButton.OK;
            MessageBox.Show(message, caption, ok, icon);
        }
    }
}
