﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TP4.ViewModel;

namespace TP4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddPlayerWindow : Window
    {
        private PlayerViewModel PVM = PlayerViewModel.Instance;
        public AddPlayerWindow()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PVM.addPlayer(this.Nom.Text, this.Prenom.Text, this.Pseudo.Text);
            this.Close();
        }
    }
}
