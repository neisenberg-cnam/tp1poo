﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Text;
using SpaceInvaderGame;

namespace TP4.ViewModel
{
    class ArmoryViewModel  : INotifyPropertyChanged
    {
        public ObservableCollection<Arme> WeaponList { get; set; }
        private Arme selectedArme;
        public Arme SelectedArme { get { return selectedArme; } set { selectedArme = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public ArmoryViewModel() {
            ObservableCollection<Arme> list = new ObservableCollection<Arme>();
            list = getWeaponList();
            WeaponList = list;
        }

        public ObservableCollection<Arme> getWeaponList()
        {
            ObservableCollection<Arme> armes = new ObservableCollection<Arme>();
            Armurerie armory = new Armurerie();
            armory.WeaponList().ForEach(delegate (Arme weap)
            {
                armes.Add(weap);
            });
            return armes;
        }
        
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
