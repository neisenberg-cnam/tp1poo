﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Text;
using SpaceInvaderGame;

namespace TP4.ViewModel
{
    class PlayerViewModel  : INotifyPropertyChanged
    {
        public ObservableCollection<Joueur> PlayerList { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly Lazy<PlayerViewModel> lazy =
        new Lazy<PlayerViewModel>(() => new PlayerViewModel());

        public static PlayerViewModel Instance { get { return lazy.Value; } }

        private PlayerViewModel() {
            ObservableCollection<Joueur> list = new ObservableCollection<Joueur>();
            PlayerList = list;
        }

        public void addPlayer(string nom, string prenom, string pseudo)
        {
            Joueur newPlayer = new Joueur(nom, prenom, pseudo);
            PlayerList.Add(newPlayer);
        }

        public ObservableCollection<Arme> getWeaponList()
        {
            ObservableCollection<Arme> armes = new ObservableCollection<Arme>();
            CurrentPlayer.Ship.WeaponList().ForEach(delegate (Arme weap)
            {
                armes.Add(weap);
            });
            return armes;
        }

        private Joueur currentPlayer;

        public Joueur CurrentPlayer
        {
            get { return currentPlayer; }
            set
            {
                currentPlayer = value;
                OnPropertyChanged();
            }
        }

        private string currentImg;

        public string CurrentImg
        {
            get { return currentImg; }
            set
            {
                currentImg = value;
                OnPropertyChanged();
            }
        }

        public void removePlayer(int index)
        {
            PlayerList.Remove(PlayerList[index]);
        }

        public void changeCurrentPlayer(int index)
        {
            CurrentPlayer = PlayerList[index];
            CurrentImg = CurrentPlayer.Ship.ImgPath;
        }

        public string getPlayerShipName(int index)
        {
            return PlayerList[index].GetVaisseau().GetType().Name;
        }

        public int getPlayerShipStruc(int index)
        {
            return PlayerList[index].GetVaisseau().CurrentStructure;
        }

        public void changeCurrentImage(string fullPath)
        {
            if(CurrentPlayer != null)
            {
                CurrentPlayer.Ship.ImgPath = fullPath;
                CurrentImg = fullPath;
            }
        }

        public void addWeaponToCurrent(int index)
        {
            if(index >= 0 && CurrentPlayer != null)
            {
                CurrentPlayer.Ship.AddWeapon(new Armurerie().GetNthWeapon(index));
            }
            
        }

        public void delWeaponToCurrent(int index)
        {
            if (index >= 0 && CurrentPlayer != null)
            {
                Arme Weap = CurrentPlayer.Ship.WeaponList()[index];
                CurrentPlayer.Ship.RemoveWeapon(Weap);
            }

        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
